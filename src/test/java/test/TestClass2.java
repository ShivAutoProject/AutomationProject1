package test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pom.NavigationPage;

public class TestClass2 extends BaseTest {
	WebDriver driver;
	NavigationPage navigationPage;

	@Test
	public void firstTest() throws InterruptedException {
		NavigationPage navigationPage = new NavigationPage(getDriver());
		navigationPage.navigateToAccountList();
		navigationPage.navigateToWishList();
		navigationPage.navigateToAmazonPay();
		navigationPage.navigateToNewReleases();
	}

}
