package test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pom.LoginPage;

public class TestClass3 extends BaseTest {

	@Test
	@Parameters({ "userName", "password", "type" })
	public void loginTest(String userName, String password, String testType) throws InterruptedException {
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.enterlogin(userName, password, testType);
	}

}
