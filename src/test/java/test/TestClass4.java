package test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pom.MobilePage;

public class TestClass4 extends BaseTest {
	WebDriver driver;
	MobilePage mobilePage;

	@Test
	public void test() throws InterruptedException, IOException {
		MobilePage mobilePage = new MobilePage(getDriver());
		mobilePage.searchProductAndAddToCart();
	}

}
