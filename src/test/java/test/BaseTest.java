package test;

import java.io.IOException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import utils.GRIDConfig;
import utils.UtilsClass;

public class BaseTest extends UtilsClass {
	protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();

	@BeforeSuite
	public void beforeSuite() throws IOException {
		GRIDConfig.beforeSuite();
	}

	@BeforeMethod
	@Parameters("browser")
	public void setupBrower(String browser) throws IOException {
		getBrowser(browser);
		launchApplication();
	}

	@AfterMethod
	public void closeBrower() {
		getDriver().quit();
	}

	@AfterSuite
	public void afterSuite() throws IOException {
		GRIDConfig.afterSuite();
	}
}
