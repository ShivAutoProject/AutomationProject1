package pom;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.UtilsClass;

public class MobilePage extends UtilsClass{
	WebDriver driver;
	String firstWindow;
	String secondWindow;
	String thirdWindow;
	public MobilePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "twotabsearchtextbox")
	public WebElement txtSearch;

	@FindBy(how = How.ID, using = "nav-search-submit-button")
	public WebElement btnSearch;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'results for')]")
	public WebElement lblSearchResultCount;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"search\"]/span/div/span/h1/div/div[1]/div/div/span[3]")
	public WebElement lblSearchResultFor;

	@FindBy(how = How.ID, using = "p_72/1318476031")
	public WebElement clickAverageReview;

	@FindBy(how = How.XPATH, using = "//*[@id=\"search\"]/div[1]/div[2]/div/span[3]/div[2]/div[3]/div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a")
	public WebElement clickFirstMobile;

	@FindBy(how = How.ID, using = "add-to-cart-button")
	public WebElement btnCart;

	@FindBy(how = How.ID, using = "priceblock_ourprice")
	public WebElement lblPrice;

	@FindBy(how = How.ID, using = "//span[@id='productTitle']")
	public WebElement searchedMobile;

	@FindBy(how = How.XPATH, using = "//*[@id='glowContextualIngressPt_feature_div']/span/a")
	public WebElement linkDelivery;

	@FindBy(how = How.XPATH, using = "//*[@id='a-popover-7']")
	public WebElement waitForPincodePopUp;
	
	@FindBy(how = How.ID, using = "GLUXZipUpdateInput")
	public WebElement txtPincode;

	@FindBy(how = How.XPATH, using = "//input[@aria-labelledby='GLUXZipUpdate-announce']")
	public WebElement btnApply;

	@FindBy(how = How.XPATH, using = "//*[@id=\"p_89/Redmi\"]/span/a/div/label/i")
	public WebElement chkBxRedmi;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"attachDisplayAddBaseAlert\"]/div/h4")
	public WebElement txtConfirmMessage;

	@FindBy(how = How.ID, using = "attach-close_sideSheet-link")
	public WebElement btnCloseCart;

	@FindBy(how = How.XPATH, using = "//*[@id=\"prodDetails\"]/div/div[1]/div[1]/div/div[1]/div/h1")
	public WebElement txtTechDetails;

	@FindBy(how = How.ID, using = "sp_hqp_shared_inner")
	public WebElement linkSponsored;

	@FindBy(how = How.ID, using = "ape_Detail_hero-quick-promo_Desktop_iframe")
	public WebElement eleFrameSponsored;
	
	@FindBy(how = How.XPATH, using = "//*[@id='glowContextualIngressPt_feature_div']/span/a/div/div/div/span/div/span[1]")
	public WebElement lblPincode1;
	
	@FindBy(how = How.XPATH, using = "//*[@id='glowContextualIngressPt_feature_div']/span/a/div/div/div/span/div/span[2]")
	public WebElement lblPincode2;
	
	@FindBy(how = How.XPATH, using = "//span[@id='productTitle']")
	public WebElement lblSearchedProduct;
	
	public void waitForTextToPresent(WebElement ele, String text) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.textToBePresentInElementValue(ele, text));
	}
	
	public void searchProductAndAddToCart() throws InterruptedException, IOException {
		searchMobile("mi mobile");
		windowhandle();
		enterPincode(getPincode());
		clickSponsored();
		iteratAndCloseSponsoredWindow();
		switchToWindow(secondWindow);
		addToCart();
		scrollToTechDetails();
	}
	
	public void searchMobile(String productName) {
		waitForvisibilityOf(txtSearch);
		txtSearch.sendKeys(productName);
		btnSearch.click();
		waitForvisibilityOf(lblSearchResultFor);
		String search=lblSearchResultFor.getText();
		Assert.assertEquals(search.substring(1, search.length()-1),productName);
		waitForvisibilityOf(clickAverageReview);
		clickAverageReview.click();
		waitForvisibilityOf(chkBxRedmi);
		chkBxRedmi.click();
		waitForvisibilityOf(clickFirstMobile);
		clickFirstMobile.click();
	}
	
	public void windowhandle() {
		firstWindow = driver.getWindowHandle();
		Set<String> s1 = driver.getWindowHandles();
		Iterator<String> i1 = s1.iterator();
		while (i1.hasNext()) {
			secondWindow= i1.next();
			if (!firstWindow.equalsIgnoreCase(secondWindow)) {
				switchToWindow(secondWindow);
			}
		}
	}
	
	public void enterPincode(String pincode) throws InterruptedException {
		Thread.sleep(10000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", linkDelivery);
		waitForvisibilityOf(txtPincode);
		js.executeScript("arguments[0].value='"+pincode+"';", txtPincode);
		Assert.assertEquals(txtPincode.getAttribute("value"),"461331");
		waitForvisibilityOf(btnApply);
		js.executeScript("arguments[0].click();", btnApply);
		Thread.sleep(8000);
		String s=lblPincode2.getText();
		Assert.assertEquals(pincode,s.substring((s.length()-7),(s.length()-1)));
		Thread.sleep(5000);
	}

	public void clickSponsored() {
		driver.switchTo().frame(eleFrameSponsored);
		linkSponsored.click();
	}
	
	public void addToCart() {
		waitForvisibilityOf(btnCart);
		btnCart.click();
		waitForvisibilityOf(txtConfirmMessage);
		Assert.assertEquals(txtConfirmMessage.getText(), "Added to Cart");
		btnCloseCart.click();
	}

	public void switchToWindow(String windowName) {
		driver.switchTo().window(windowName);
	}
	
	public void closeToWindow(String windowName) {
		driver.switchTo().window(windowName).close();
	}
	
	public void iteratAndCloseSponsoredWindow() {
		Set<String> s2 = driver.getWindowHandles();
		Iterator<String> i2 = s2.iterator();
		while (i2.hasNext()) {
			thirdWindow = i2.next();
			if (!secondWindow.equalsIgnoreCase(thirdWindow) && !firstWindow.equalsIgnoreCase(thirdWindow)) {
				driver.switchTo().window(thirdWindow).close();;
			}
		}
	}

	public void scrollToTechDetails() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", txtTechDetails);
	}
}
