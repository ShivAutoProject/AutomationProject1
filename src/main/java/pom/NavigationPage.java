package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utils.UtilsClass;

public class NavigationPage extends UtilsClass {
	WebDriver driver;

	public NavigationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Account & Lists')]")
	public WebElement linkAccountList;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create a Wish List')]")
	public WebElement linkCreateWishList;

	@FindBy(how = How.LINK_TEXT, using = "Your Lists")
	public WebElement linkYourList;

	@FindBy(how = How.LINK_TEXT, using = "Amazon Pay")
	public WebElement linkAmazonPay;

	@FindBy(how = How.LINK_TEXT, using = "New Releases")
	public WebElement linkNewReleases;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Amazon Hot New Releases')]")
	public WebElement txtAmazonRelease;

	@FindBy(how = How.ID, using = "twotabsearchtextbox")
	public WebElement txtSearch;

	@FindBy(how = How.ID, using = "nav-search-submit-button")
	public WebElement btnSearch;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Sign Out')]")
	public WebElement linkSignOut;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Deliver to')]")
	public WebElement lblSignOut;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Amazon Pay balance')]")
	public WebElement lblAmazonPay;
	
	public void navigateToAccountList() throws InterruptedException {
		mouserOverUsingActionClass(linkAccountList);
	}

	public void navigateToWishList() {
		waitForvisibilityOf(linkCreateWishList);
		clickUsingActionClass(linkCreateWishList);
		waitForvisibilityOf(linkYourList);
		Assert.assertEquals(linkYourList.getText(), "Your Lists");
	}

	public void navigateToAmazonPay() {
		waitForvisibilityOf(linkAmazonPay);
		clickUsingActionClass(linkAmazonPay);
		waitForvisibilityOf(lblAmazonPay);
		Assert.assertEquals(lblAmazonPay.getText(), "Amazon Pay balance");
	}

	public void navigateToNewReleases() {
		clickUsingActionClass(linkNewReleases);
		waitForvisibilityOf(txtAmazonRelease);
		Assert.assertEquals(txtAmazonRelease.getText(), "Amazon Hot New Releases");
	}
	
	public void logout() throws InterruptedException {
		navigateToAccountList();
		waitForvisibilityOf(lblSignOut);
		clickUsingActionClass(linkSignOut);
	}
	
}
