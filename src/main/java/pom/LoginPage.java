package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import utils.UtilsClass;

public class LoginPage extends UtilsClass{
	WebDriver driver;
	NavigationPage navigationPage;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		navigationPage = new NavigationPage(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "nav-link-accountList-nav-line-1")
	public WebElement linkLogin;

	@FindBy(how = How.ID, using = "ap_email")
	public WebElement txtEmail;

	@FindBy(how = How.ID, using = "continue")
	public WebElement btnContinue;

	@FindBy(how = How.NAME, using = "password")
	public WebElement txtPwd;

	@FindBy(how = How.ID, using = "signInSubmit")
	public WebElement btnLogin;

	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'There was a problem')]")
	public WebElement lblInvalid;
	
	@FindBy(how = How.XPATH, using = "//*[@id='nav-link-accountList-nav-line-1']")
	public WebElement lblUser;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Hello, Sign in')]")
	public WebElement lblSignIn;
	
	public void enterlogin(String userName, String password, String testType) throws InterruptedException {
		waitForvisibilityOf(linkLogin);
		linkLogin.click();
		txtEmail.sendKeys(userName);
		btnContinue.click();
		txtPwd.sendKeys(password);
		btnLogin.click();
		if (testType.equalsIgnoreCase("valid")) {
				navigationPage.logout();
		} else {
			waitForvisibilityOf(lblInvalid);
			Assert.assertEquals(lblInvalid.getText(), "There was a problem");
		}
	}


		
	
}
