package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class UtilsClass {
	protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();
	Actions action;

	public static Properties readPropertiesFile() throws IOException {
		FileInputStream fis = null;
		Properties prop = null;
		try {
			fis = new FileInputStream("cofig.properties");
			prop = new Properties();
			prop.load(fis);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			fis.close();
		}
		return prop;
	}

	public WebDriver getBrowser(String browser) throws IOException {
		InetAddress localhost = InetAddress.getLocalHost();
		String hubURL = localhost.getHostAddress().trim();
		if (browser.equalsIgnoreCase("chrome")) {
			ChromeOptions chromeOptions = new ChromeOptions();
			driver.set(new RemoteWebDriver(new URL("http://" + hubURL + ":4444/wd/hub"), chromeOptions));
		} else if (browser.equalsIgnoreCase("firefox")) {
			FirefoxOptions firefoxOptions = new FirefoxOptions();
			driver.set(new RemoteWebDriver(new URL("http://" + hubURL + ":4444/wd/hub"), firefoxOptions));
		}
		getDriver().manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		getDriver().manage().window().maximize();
		return driver.get();
	}

	public WebDriver getDriver() {
		return driver.get();
	}
	
	public void verifyCurrentURL() throws IOException
	{
		Assert.assertEquals(getUrl(), getDriver().getCurrentUrl());
	}
	
	public void verifyPageTitle() throws IOException
	{
		Assert.assertEquals(getExpectedTitle(), getDriver().getTitle());
	}

	public String getUrl() throws IOException {
		String url = readPropertiesFile().getProperty("url");
		return url;
	}
	
	public void launchApplication() throws IOException {
		getDriver().get(getUrl());
		verifyCurrentURL();
		verifyPageTitle();
	}
	public String getExpectedTitle() throws IOException {
		String title = readPropertiesFile().getProperty("title");
		return title;
	}
	
	public String getPincode() throws IOException {
		String pincode = readPropertiesFile().getProperty("pincode");
		return pincode;
	}

	public void waitForvisibilityOf(WebElement ele) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.visibilityOf(ele));
	}

	public void waitForElementClickable(WebElement ele) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 60);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	public void clickUsingActionClass(WebElement ele) {
		waitForvisibilityOf(ele);
		action = new Actions(getDriver());
		action.moveToElement(ele).click().build().perform();
	}

	public void mouserOverUsingActionClass(WebElement ele) throws InterruptedException {
		waitForvisibilityOf(ele);
		Thread.sleep(5000);
		action = new Actions(getDriver());
		action.moveToElement(ele).build().perform();
	}

}
