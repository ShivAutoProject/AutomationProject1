package utils;

import java.io.File;
import java.io.IOException;

public class GRIDConfig {
	static Process hub;
	static Process chromeNode;
	static Process firefoxNode;

	public static void beforeSuite() throws IOException {
		System.out.println("suit invoked");
		ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "hub.bat");
		ProcessBuilder chromeNodePb = new ProcessBuilder("cmd", "/c", "chrome.bat");
		ProcessBuilder firefoxNodePb = new ProcessBuilder("cmd", "/c", "gecko.bat");
		File dir = new File("GRID");
		pb.directory(dir);
		chromeNodePb.directory(dir);
		firefoxNodePb.directory(dir);
		hub = pb.start();
		chromeNode = chromeNodePb.start();
		firefoxNode = firefoxNodePb.start();
	}

	public static void afterSuite() {
		hub.destroyForcibly();
		chromeNode.destroyForcibly();
		firefoxNode.destroyForcibly();
	}

}
